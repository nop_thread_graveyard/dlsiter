CREATE TABLE works (
	work_id_internal INTEGER PRIMARY KEY,
	id TEXT UNIQUE NOT NULL
);
CREATE UNIQUE INDEX index_works_id ON works(id);

CREATE TABLE purchases (
	purchase_id INTEGER PRIMARY KEY,
	work_id_internal INTEGER UNIQUE REFERENCES works(work_id_internal),
	purchased_at TEXT,
	purchased_at_is_precise BOOLEAN NOT NULL DEFAULT 0,
	price INTEGER
);
CREATE UNIQUE INDEX index_purchases_work ON purchases(work_id_internal);
CREATE INDEX index_purchases_price ON purchases(price);
