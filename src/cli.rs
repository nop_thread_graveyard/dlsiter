//! CLI.

use std::path::PathBuf;
use std::str;

use clap::Clap;

/// CLI options.
#[derive(Debug, Clone, Clap)]
pub enum CliOpt {
    /// Fetches the personal data.
    Fetch(Fetch),
    /// Query the data and statistics.
    Query(Query),
}

/// Fetches the purchase history.
#[derive(Debug, Clone, Clap)]
pub struct Fetch {
    /// Config file.
    #[clap(parse(from_os_str), short, long, default_value = "./dlsiter.toml")]
    pub(crate) config: PathBuf,
    /// DB file.
    #[clap(parse(from_os_str), short, long, default_value = "./dlsiter.sqlite3")]
    pub(crate) db: PathBuf,
    /// Fetch all history even when some data have already been fetched.
    #[clap(long, short, long)]
    pub(crate) all: bool,
}

/// Query the data and statistics.
#[derive(Debug, Clone, Clap)]
pub struct Query {
    /// Config file.
    #[clap(parse(from_os_str), short, long, default_value = "./dlsiter.toml")]
    pub(crate) config: PathBuf,
    /// DB file.
    #[clap(parse(from_os_str), short, long, default_value = "./dlsiter.sqlite3")]
    pub(crate) db: PathBuf,
    /// Format.
    #[clap(parse(try_from_str), short, long, possible_values = Format::variants())]
    pub(crate) format: Format,
    /// Output file.
    ///
    /// If `-` is specified, the output is written to the standard output.
    #[clap(parse(from_os_str), short, long)]
    pub(crate) output: Option<PathBuf>,
    /// Query target.
    #[clap(parse(try_from_str), possible_values = QueryTarget::variants())]
    pub(crate) target: QueryTarget,
}

/// Query target.
#[derive(Debug, Clone, Clap)]
pub enum QueryTarget {
    /// Query the purchased works.
    Works,
}

impl QueryTarget {
    fn variants() -> &'static [&'static str] {
        &["works"]
    }
}

impl str::FromStr for QueryTarget {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "works" => Ok(Self::Works),
            _ => Err(anyhow::anyhow!("Unknown query target {:?}", s)),
        }
    }
}

#[derive(Debug, Clone, Clap)]
pub enum Format {
    /// CSV.
    Csv,
}

impl Format {
    fn variants() -> &'static [&'static str] {
        &["csv"]
    }
}

impl str::FromStr for Format {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "csv" => Ok(Self::Csv),
            _ => Err(anyhow::anyhow!("Unknown format {:?}", s)),
        }
    }
}
