//! DLsite history manager.
#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]

pub mod cli;
pub mod config;
pub mod db;
pub mod dlsite;

use std::path::Path;

use anyhow::Context;
use clap::Clap;
use futures_util::future;
use futures_util::stream::TryStreamExt;

use self::cli::CliOpt;
use self::config::Config;
use self::db::Db;
use self::dlsite::DlsiteClient;

#[async_std::main]
async fn main() -> Result<(), anyhow::Error> {
    init_logger();

    let opt = CliOpt::parse();
    log::debug!("opt = {:?}", opt);

    match opt {
        CliOpt::Fetch(fetch) => {
            let config = Config::load_file(&fetch.config).await?;
            let db = Db::new(&fetch.db).await?;
            let dlsite = DlsiteClient::with_session_id(config.session_id().clone());

            let fetch_duration = if fetch.all {
                dlsite::purchase::YearMonthDuration::default()
            } else {
                let start_datetime = db
                    .query_most_recent_purchase_datetime_with_price_info()
                    .await?;
                let start_yearmonth = start_datetime.map(dlsite::purchase::YearMonth::from);
                dlsite::purchase::YearMonthDuration {
                    start: start_yearmonth,
                    end: None,
                }
            };
            log::debug!("Duration to fetch: {:?}", fetch_duration);
            let num_updated = dlsite
                .fetch_purchase_pages_during(fetch_duration)
                .try_fold(0_usize, |count, page| {
                    log::trace!(
                        "Processing purchase page {}/{}",
                        page.current_page,
                        page.total_pages
                    );
                    let db = db.clone();
                    async move {
                        let num_affected =
                            db.save_purchase_prices(page.items.prices_iter()).await?;
                        Ok(count + num_affected)
                    }
                })
                .await?;
            if num_updated == 0 {
                log::info!("No entries are updated");
            } else {
                log::info!("Updated {} entries", num_updated);
            }
        }
        CliOpt::Query(query) => {
            use crate::cli::{Format, QueryTarget};

            let db = Db::new(&query.db).await?;

            let (mut file, stdout): (std::fs::File, std::io::Stdout);
            let mut stdout_lock: std::io::StdoutLock<'_>;
            let output: &mut dyn std::io::Write;
            match query.output {
                Some(path) if path != Path::new("-") => {
                    file = std::fs::File::create(path)?;
                    output = &mut file;
                }
                None | Some(_) => {
                    stdout = std::io::stdout();
                    stdout_lock = stdout.lock();
                    output = &mut stdout_lock;
                }
            }
            match query.target {
                QueryTarget::Works => match query.format {
                    Format::Csv => {
                        let mut writer = csv::Writer::from_writer(output);
                        db.query_purchased_works()
                            .try_for_each(|item| {
                                future::ready(
                                    writer.serialize(item).context("Failed to write CSV row"),
                                )
                            })
                            .await?;
                    }
                },
            }
        }
    }

    Ok(())
}

/// Initialize logger.
fn init_logger() {
    /// Default log filter for debug build.
    #[cfg(debug_assertions)]
    const DEFAULT_LOG_FILTER: &str = "dlsiter=debug";
    /// Default log filter for release build.
    #[cfg(not(debug_assertions))]
    const DEFAULT_LOG_FILTER: &str = "dlsiter=warn";

    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or(DEFAULT_LOG_FILTER))
        .init();
}
