//! Config.

use std::io;
use std::path::Path;

use serde::Deserialize;

use crate::dlsite::SessionId;

/// Config.
#[derive(Debug, Deserialize)]
pub struct Config {
    /// Credential.
    credential: Credential,
}

impl Config {
    /// Returns the sessison ID.
    pub fn session_id(&self) -> &SessionId {
        &self.credential.session_id
    }

    /// Loads a config from a file at the given path.
    pub async fn load_file<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        Self::load_file_impl(path.as_ref()).await
    }

    async fn load_file_impl(path: &Path) -> io::Result<Self> {
        let content = async_std::fs::read_to_string(path).await?;
        toml::from_str(&content).map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
    }
}

/// Credential.
#[derive(Debug, Deserialize)]
pub struct Credential {
    /// Session ID.
    session_id: SessionId,
}
