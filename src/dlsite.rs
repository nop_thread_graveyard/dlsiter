//! DLsite API.

pub(crate) mod purchase;

use std::convert::TryFrom;
use std::fmt;
use std::num::NonZeroU16;
use std::sync::Arc;

use async_std::stream::Stream;
use serde::Deserialize;
use surf::http::headers::HeaderValue;
use surf::http::Cookie;

pub use self::purchase::{PurchasePageDocument, PurchasePriceRef, YearMonthDuration};

/// Session ID.
#[derive(Clone, Deserialize)]
#[serde(transparent)]
pub struct SessionId(String);

impl SessionId {
    /// Creates a new session ID.
    pub fn new(id: String) -> Self {
        Self(id)
    }

    /// Returns the raw session ID.
    fn raw(&self) -> &str {
        &self.0
    }

    /// Creates a cookie for the session ID.
    fn to_cookie(&self) -> Cookie<'_> {
        Cookie::new("__DLsite_SID", self.raw())
    }

    /// Creates a cookie for the session ID.
    fn to_cookie_header(&self) -> HeaderValue {
        self.to_cookie().into()
    }
}

impl fmt::Debug for SessionId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("SessionId").field(&"(credential)").finish()
    }
}

/// Pagination config.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Pagination {
    /// Size.
    size: usize,
}

impl Pagination {
    /// Creates a new pagination from the given page size.
    pub fn new(size: usize) -> Self {
        Self { size }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PaginatedItems<T> {
    /// Items for the page.
    pub(crate) items: T,
    /// Current page index.
    pub(crate) current_page: PageIndex,
    /// Number of total pages.
    pub(crate) total_pages: u16,
}

impl<T> PaginatedItems<T> {
    pub fn map<U, F>(self, f: F) -> PaginatedItems<U>
    where
        F: FnOnce(T) -> U,
    {
        PaginatedItems {
            items: f(self.items),
            current_page: self.current_page,
            total_pages: self.total_pages,
        }
    }
}

/// Index of a page.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PageIndex {
    index1: NonZeroU16,
}

impl PageIndex {
    /// Creates a `PageIndex` from the given zero-based index.
    ///
    /// # Panics
    ///
    /// Panics if the given page number is too large.
    pub fn with_index0(index0: usize) -> Self {
        match u16::try_from(index0)
            .ok()
            .and_then(|v| NonZeroU16::new(v.wrapping_add(1)))
        {
            Some(index1) => Self { index1 },
            None => panic!("Page index {:?} (0-based) is too large", index0),
        }
    }

    pub fn index1(&self) -> usize {
        usize::from(self.index1.get())
    }

    /// Returns the next page index.
    ///
    /// # Panics
    ///
    /// Panics if the page number overflows.
    pub fn next_page(self) -> Self {
        match NonZeroU16::new(self.index1.get().wrapping_add(1)) {
            Some(index1) => Self { index1 },
            None => panic!("Page number overflowed"),
        }
    }
}

impl Default for PageIndex {
    fn default() -> Self {
        Self {
            index1: NonZeroU16::new(1).expect("Should never fail: 1 is nonzero"),
        }
    }
}

impl fmt::Display for PageIndex {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.index1().fmt(f)
    }
}

/// DLsite client.
#[derive(Debug, Clone)]
pub struct DlsiteClient {
    /// Session ID.
    session_id: Arc<SessionId>,
}

impl DlsiteClient {
    /// Creates a new `DlsiteClient` from the given session ID.
    pub fn with_session_id(session_id: SessionId) -> Self {
        Self {
            session_id: Arc::new(session_id),
        }
    }

    pub fn fetch_purchase_pages_during(
        &self,
        duration: YearMonthDuration,
    ) -> impl Stream<Item = anyhow::Result<PaginatedItems<PurchasePageDocument>>> {
        PurchasePageDocument::fetch_purchase_pages_during(&self.session_id, duration)
    }
}
