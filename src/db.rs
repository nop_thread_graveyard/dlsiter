//! App data store.

use std::path::Path;
use std::sync::Arc;

use anyhow::Context;
use chrono::{DateTime, FixedOffset};
use futures_util::future;
use futures_util::stream::{Stream, TryStreamExt};
use serde::{Deserialize, Serialize};
use sqlx::sqlite::SqliteConnectOptions;
use sqlx::SqlitePool;

use crate::dlsite::PurchasePriceRef;

/// Database.
#[derive(Debug, Clone)]
pub struct Db {
    /// Database connection pool.
    pool: Arc<SqlitePool>,
}

impl Db {
    pub async fn new<S: AsRef<Path>>(filename: S) -> anyhow::Result<Self> {
        Self::new_impl(filename.as_ref()).await
    }

    async fn new_impl(filename: &Path) -> anyhow::Result<Self> {
        let pool = {
            let opts = SqliteConnectOptions::new()
                .filename(filename)
                .create_if_missing(true);
            SqlitePool::connect_with(opts).await.with_context(|| {
                format!(
                    "Failed to connect to SQLite database {}",
                    filename.display()
                )
            })?
        };

        let mut migrator = sqlx::migrate!("./migrations");
        // Temporary workaround: remove reverse migrations.
        // See <https://github.com/launchbadge/sqlx/issues/863>.
        migrator
            .migrations
            .to_mut()
            .retain(|migration| !migration.migration_type.is_down_migration());
        migrator
            .run(&pool)
            .await
            .context("Failed to migrate the database")?;

        Ok(Self {
            pool: Arc::new(pool),
        })
    }

    /// Saves the given prices to the DB, and returns the number of updated entries.
    pub async fn save_purchase_prices<'a, I>(&self, iter: I) -> anyhow::Result<usize>
    where
        I: IntoIterator<Item = PurchasePriceRef<'a>>,
    {
        let mut affected = 0_usize;
        let mut xaction = self
            .pool
            .begin()
            .await
            .context("Failed to start transaction")?;
        for purchase in iter {
            let work_id = purchase.work_id;
            log::trace!("Saving purchase entry for {:?}", work_id);
            let price = purchase.price as i32;
            let datetime = purchase.datetime;
            let done = sqlx::query!(
                " \
                    INSERT OR IGNORE INTO works (id) \
                    VALUES (?); \
                    INSERT INTO purchases (work_id_internal, price, purchased_at) \
                    SELECT work_id_internal, ?, ? \
                    FROM works \
                    WHERE works.id = ? \
                    ON CONFLICT (work_id_internal) DO UPDATE \
                    SET price = ?, purchased_at = COALESCE(purchased_at, ?) \
                    WHERE price IS NULL; \
                ",
                work_id,
                price,
                datetime,
                work_id,
                price,
                datetime
            )
            .execute(&mut xaction)
            .await
            .context("Failed to insert purchase price")?;
            affected += done.rows_affected() as usize;
        }
        xaction
            .commit()
            .await
            .context("Failed to commit the transaction")?;
        Ok(affected)
    }

    pub async fn query_most_recent_purchase_datetime_with_price_info(
        &self,
    ) -> anyhow::Result<Option<chrono::DateTime<chrono::FixedOffset>>> {
        let row = sqlx::query!(
            " \
                SELECT purchased_at as \"purchased_at!: DateTime<FixedOffset>\" \
                FROM purchases \
                WHERE (price IS NOT NULL) AND (purchased_at IS NOT NULL) \
                ORDER BY purchased_at DESC \
                LIMIT 1 \
            "
        )
        .fetch_optional(&*self.pool)
        .await
        .context("Failed to get most recent purchase entry in the database")?;
        let jp_offset = chrono::FixedOffset::east(9 * 3600);
        Ok(row.map(|record| record.purchased_at.with_timezone(&jp_offset)))
    }

    pub fn query_purchased_works(&self) -> impl Stream<Item = anyhow::Result<PurchasedWork>> + '_ {
        let jp_offset = chrono::FixedOffset::east(9 * 3600);
        sqlx::query_as!(
            PurchasedWork,
            " \
                SELECT \
                    works.id as \"work_id\", \
                    price as \"purchase_price: i64\", \
                    purchased_at as \"purchased_at: chrono::DateTime<chrono::FixedOffset>\", \
                    purchased_at_is_precise \
                FROM works \
                INNER JOIN purchases USING (work_id_internal) \
            "
        )
        .fetch_many(&*self.pool)
        .try_filter_map(|row| future::ready(Ok(row.right())))
        .map_ok(move |mut entry| {
            if let Some(datetime) = &mut entry.purchased_at {
                *datetime = datetime.with_timezone(&jp_offset);
            }
            entry
        })
        .map_err(|e| e.into())
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedWork {
    /// Work ID (workno).
    work_id: String,
    /// Purchase price.
    purchase_price: Option<i64>,
    purchased_at: Option<chrono::DateTime<chrono::FixedOffset>>,
    purchased_at_is_precise: bool,
}
