//! Purchase data.

use std::convert::TryFrom;
use std::fmt;
use std::iter;
use std::num::NonZeroU16;
use std::ops::{self, Bound};

use anyhow::{bail, Context};
use chrono::Datelike;
use futures_util::stream::{self, Stream};
use select::document::Document;
use select::predicate as pred;
use surf::http::mime;

use crate::dlsite::{PageIndex, PaginatedItems, SessionId};

type PriceRowPredicate = pred::Descendant<
    pred::Child<pred::Attr<&'static str, &'static str>, pred::Name<&'static str>>,
    pred::And<pred::Name<&'static str>, pred::Not<pred::Class<&'static str>>>,
>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct YearMonth {
    year: u16,
    month1: u8,
}

impl YearMonth {
    #[inline]
    pub fn new(year: u16, month1: u8) -> Self {
        Self { year, month1 }
    }

    fn next_month(self) -> Self {
        if self.month1 == 12 {
            Self {
                year: self.year + 1,
                month1: 1,
            }
        } else {
            Self {
                month1: self.month1 + 1,
                ..self
            }
        }
    }

    fn prev_month(self) -> Self {
        if self.month1 == 1 {
            Self {
                year: self.year - 1,
                month1: 12,
            }
        } else {
            Self {
                month1: self.month1 - 1,
                ..self
            }
        }
    }
}

impl From<&chrono::NaiveDate> for YearMonth {
    fn from(date: &chrono::NaiveDate) -> Self {
        let year = u16::try_from(date.year() as u32)
            .unwrap_or_else(|_| panic!("Unexpected datetime {:?}", date));
        Self {
            year,
            month1: date.month() as u8,
        }
    }
}

impl From<chrono::NaiveDate> for YearMonth {
    #[inline]
    fn from(date: chrono::NaiveDate) -> Self {
        Self::from(&date)
    }
}

impl<Tz: chrono::TimeZone> From<&chrono::Date<Tz>> for YearMonth {
    fn from(date: &chrono::Date<Tz>) -> Self {
        let jp_offset = chrono::FixedOffset::east(9 * 3600);
        let naive_local = date.with_timezone(&jp_offset).naive_local();
        Self::from(&naive_local)
    }
}

impl<Tz: chrono::TimeZone> From<chrono::Date<Tz>> for YearMonth {
    #[inline]
    fn from(date: chrono::Date<Tz>) -> Self {
        Self::from(&date)
    }
}

impl<Tz: chrono::TimeZone> From<&chrono::DateTime<Tz>> for YearMonth {
    fn from(datetime: &chrono::DateTime<Tz>) -> Self {
        Self::from(&datetime.date())
    }
}

impl<Tz: chrono::TimeZone> From<chrono::DateTime<Tz>> for YearMonth {
    fn from(datetime: chrono::DateTime<Tz>) -> Self {
        Self::from(&datetime.date())
    }
}

impl fmt::Display for YearMonth {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:04}-{:02}", self.year, self.month1)
    }
}

/// Inclusive range.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub struct YearMonthDuration {
    pub(crate) start: Option<YearMonth>,
    pub(crate) end: Option<YearMonth>,
}

impl<T: ops::RangeBounds<YearMonth>> From<T> for YearMonthDuration {
    fn from(range: T) -> Self {
        let start = match range.start_bound() {
            Bound::Included(v) => Some(*v),
            Bound::Excluded(v) => Some(v.next_month()),
            Bound::Unbounded => None,
        };
        let end = match range.end_bound() {
            Bound::Included(v) => Some(*v),
            Bound::Excluded(v) => Some(v.prev_month()),
            Bound::Unbounded => None,
        };
        Self { start, end }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub enum SortOrder {
    // Currently unused from binary.
    #[allow(dead_code)]
    RecentFirst,
    EarliestFirst,
}

impl SortOrder {
    fn to_code(self) -> u8 {
        match self {
            Self::RecentFirst => 1,
            Self::EarliestFirst => 2,
        }
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct PurchasePageUriBuilder {
    duration: YearMonthDuration,
    order: Option<SortOrder>,
    page: PageIndex,
}

impl PurchasePageUriBuilder {
    pub fn with_order(order: SortOrder) -> Self {
        Self {
            order: Some(order),
            ..Self::default()
        }
    }

    #[inline]
    pub fn duration(mut self, duration: YearMonthDuration) -> Self {
        self.duration = duration;
        self
    }

    pub fn try_build(self) -> anyhow::Result<PurchasePageUri> {
        let order = self
            .order
            .ok_or_else(|| anyhow::anyhow!("Sort order must be set"))?;
        Ok(PurchasePageUri {
            duration: self.duration,
            order,
            page: self.page,
        })
    }

    pub fn build(self) -> PurchasePageUri {
        match self.try_build() {
            Ok(v) => v,
            Err(e) => panic!("Failed to build purchase page URI: {}", e),
        }
    }
}

impl From<PurchasePageUri> for PurchasePageUriBuilder {
    fn from(v: PurchasePageUri) -> Self {
        Self {
            duration: v.duration,
            order: Some(v.order),
            page: v.page,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct PurchasePageUri {
    duration: YearMonthDuration,
    order: SortOrder,
    page: PageIndex,
}

impl PurchasePageUri {
    #[inline]
    pub fn page(&self) -> PageIndex {
        self.page
    }

    #[inline]
    pub fn increment_page_index(&mut self) {
        self.page = self.page.next_page();
    }
}

impl fmt::Display for PurchasePageUri {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const BASE: &str = "https://www.dlsite.com/home/mypage/userbuy/=";
        // Base URI for purchase pages.
        f.write_str(BASE)?;
        // All media types.
        f.write_str("/type/all")?;
        // Duration.
        match (self.duration.start, self.duration.end) {
            (Some(start), Some(end)) => write!(f, "/start/{}/start_to_end/1/end/{}", start, end),
            (Some(start), None) => write!(f, "/start/{}/start_to_end/1", start),
            (None, Some(end)) => write!(f, "/start/{}/start_to_end/2", end),
            (None, None) => f.write_str("/start/all"),
        }?;
        // Sort order.
        let sort_order_code = self.order.to_code();
        write!(f, "/sort/{}/order/{}", sort_order_code, sort_order_code)?;
        // Page index.
        write!(f, "/page/{}", self.page)?;

        Ok(())
    }
}

/// A wrapper for HTML document of a purchase page.
pub struct PurchasePageDocument {
    /// HTML document.
    doc: Document,
}

impl PurchasePageDocument {
    /// Creates a new `PurchasePageDocument` from the given HTML source.
    fn from_html(html: &str) -> Self {
        Self {
            doc: Document::from(html),
        }
    }

    pub(crate) fn fetch_purchase_pages_during(
        session_id: &SessionId,
        duration: YearMonthDuration,
    ) -> impl Stream<Item = anyhow::Result<PaginatedItems<PurchasePageDocument>>> {
        let session_id = session_id.clone();

        #[derive(Debug, Clone, Copy)]
        struct State {
            uri: PurchasePageUri,
            total_pages: Option<NonZeroU16>,
        }
        let initial_state = State {
            // Using `EarliestFirst` order in order to prevent already fetched pages
            // from being changed by new purchase.
            uri: PurchasePageUriBuilder::with_order(SortOrder::EarliestFirst)
                .duration(duration)
                .build(),
            total_pages: None,
        };

        stream::try_unfold(initial_state, move |mut state| {
            let session_id = session_id.clone();
            async move {
                let current_page = state.uri.page;
                if state
                    .total_pages
                    .map_or(false, |total| current_page.index1() as u16 > total.get())
                {
                    return Ok(None);
                }
                let page = Self::fetch(&session_id, &state.uri)
                    .await
                    .context("Failed to get the first purchase page")?;
                let total_pages = match state.total_pages {
                    Some(v) => v,
                    None => NonZeroU16::new(page.total_pages()?)
                        .expect("Should never fail: total pages should not be zero"),
                };
                let items = PaginatedItems {
                    items: page,
                    current_page,
                    total_pages: total_pages.get(),
                };

                state.uri.increment_page_index();
                state.total_pages = Some(total_pages);
                Ok(Some((items, state)))
            }
        })
    }

    async fn fetch(session_id: &SessionId, uri: &PurchasePageUri) -> anyhow::Result<Self> {
        let req = surf::get(uri.to_string())
            .header("Cookie", session_id.to_cookie_header())
            .content_type(mime::HTML);
        // About `.map_err(|e| anyhow::anyhow!(e))`, see
        // <https://github.com/http-rs/surf/issues/86>.
        log::trace!("Fetching URI {}", uri);
        let mut response = req
            .send()
            .await
            .map_err(|e| anyhow::anyhow!(e))
            .with_context(|| format!("Failed to fetch {}-th purchase page", uri.page()))?;
        if !response.status().is_success() {
            bail!(
                "Failed to fetch {}-th purchase page (status code is {})",
                uri.page(),
                response.status()
            );
        }
        let html = response
            .body_string()
            .await
            .map_err(|e| anyhow::anyhow!(e))
            .with_context(|| {
                format!(
                    "Failed to get response body for {}-th purchase page",
                    uri.page()
                )
            })?;
        log::trace!("Successufully fetched URI {}", uri);
        Ok(Self::from_html(html.as_str()))
    }

    /// Returns the number of total pages.
    fn total_pages(&self) -> anyhow::Result<u16> {
        use select::predicate::{Class, Name, Predicate};

        let nodes = self.doc.select(
            Name("table")
                .and(Class("global_pagination"))
                .descendant(Name("td").and(Class("page_no")))
                .descendant(Name("a")),
        );
        let total_pages = match nodes.last() {
            Some(anchor) => {
                let value = anchor
                    .attr("data-value")
                    .ok_or_else(|| {
                        anyhow::anyhow!(
                            "Expected attribute `data-value` not found \
                                for the anchor to the last page"
                        )
                    })?
                    .trim();
                value.parse().with_context(|| {
                    format!("Failed to parse the last page number `{:?}`", value)
                })?
            }
            // If no pagination is available, there is only single page.
            None => 1,
        };

        Ok(total_pages)
    }

    pub fn prices_iter(&self) -> PricesIter<'_> {
        PricesIter::new(&self.doc)
    }
}

/// Iterator of prices.
#[derive(Debug)]
pub struct PricesIter<'a> {
    /// Iterator of rows which contains prices.
    row_iter: iter::Enumerate<select::document::Select<'a, PriceRowPredicate>>,
}

impl<'a> PricesIter<'a> {
    fn new(doc: &'a Document) -> Self {
        use select::predicate::{Attr, Class, Name, Predicate};

        let selector: PriceRowPredicate = Attr("id", "buy_history_this")
            .child(Name("table"))
            .descendant(Name("tr").and(Class("item_name").not()));
        Self {
            row_iter: doc.select(selector).enumerate(),
        }
    }
}

impl<'a> Iterator for PricesIter<'a> {
    type Item = PurchasePriceRef<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        for (i, row) in &mut self.row_iter {
            match PurchasePriceRef::try_from_row(&row) {
                Ok(v) => return Some(v),
                Err(e) => {
                    log::warn!("Skipping the {}-th row due to parsing failure: {}", i, e);
                    continue;
                }
            }
        }

        None
    }
}

#[derive(Debug, Clone)]
pub struct PurchasePriceRef<'a> {
    /// Work ID.
    pub(crate) work_id: &'a str,
    /// Purchase price.
    pub(crate) price: u32,
    /// Purchase datetime (not so precise).
    pub(crate) datetime: chrono::DateTime<chrono::FixedOffset>,
}

impl<'a> PurchasePriceRef<'a> {
    fn try_from_row(row: &select::node::Node<'a>) -> anyhow::Result<Self> {
        use chrono::TimeZone;
        use select::predicate::{Class, Name, Predicate};

        let work_id = find_work_id(row).context("Failed to get work ID")?;
        let datetime = {
            let text = row
                .select(Name("td").and(Class("buy_date")))
                .next()
                .ok_or_else(|| anyhow::anyhow!("Purchase datetime was not found"))?
                .text();
            let text = text.trim();
            let jp_datetime = chrono::NaiveDateTime::parse_from_str(text, "%Y/%m/%d %H:%M")
                .with_context(|| format!("Failed to parse datetime {:?}", text))?;
            let jp_offset = chrono::FixedOffset::east(9 * 3600);
            jp_offset.from_local_datetime(&jp_datetime).single().expect(
                "Should never fail because the fixed offset does not make datetime ambiguous",
            )
        };
        let price = {
            // Note that this is the price after the discount is applied, but before coupons are applied.
            let mut text = row
                .select(Name("td").and(Class("work_price")))
                .next()
                .ok_or_else(|| anyhow::anyhow!("Failed to get work price"))?
                .text();
            text.retain(|c| c.is_ascii_digit());
            text.as_str()
                .parse()
                .with_context(|| format!("Failed to parse work price {:?}", text))?
        };

        Ok(Self {
            work_id,
            price,
            datetime,
        })
    }
}

fn find_work_id<'a>(row: &select::node::Node<'a>) -> anyhow::Result<&'a str> {
    use select::predicate::{Class, Name, Predicate};

    let href = row
        .select(Name("dt").and(Class("work_name")).child(Name("a")))
        .next()
        .and_then(|anchor| anchor.attr("href"));
    // If the work is still available, the work name is a link to the work page.
    if let Some(href) = href {
        // The URI wourd be `https://www.dlsite.com/maniax/work/=/product_id/{{workno}}.html`.
        let (prefix_last, suffix_first) = href
            .rfind('/')
            .zip(href.rfind('.'))
            .filter(|(prefix_last, suffix_first)| prefix_last < suffix_first)
            .ok_or_else(|| {
                anyhow::anyhow!("Failed to parse a work ID from the store URI {:?}", href)
            })?;
        return Ok(&href[(prefix_last + 1)..suffix_first]);
    }
    log::trace!("Store URI is not available");

    let dl_uri = row
        .select(Name("a").and(Class("btn_dl")).and(Class("disabled").not()))
        .next()
        .and_then(|anchor| anchor.attr("href"));
    // If the work is not available for sale, download link for the item will be
    // still available.
    if let Some(dl_uri) = dl_uri {
        // The URI would be `https://www.dlsite.com/home/download/=/product_id/{{workno}}.html`
        let (prefix_last, suffix_first) = dl_uri
            .rfind('/')
            .zip(dl_uri.rfind('.'))
            .filter(|(prefix_last, suffix_first)| prefix_last < suffix_first)
            .ok_or_else(|| {
                anyhow::anyhow!(
                    "Failed to parse a work ID from the download URI {:?}",
                    dl_uri
                )
            })?;
        return Ok(&dl_uri[(prefix_last + 1)..suffix_first]);
    }
    log::trace!("Download URI is not available");

    let play_uri = row
        .select(Name("a").and(Class("btn_st")).and(Class("disabled").not()))
        .next()
        .and_then(|anchor| anchor.attr("href"));
    // If the work is not available for sale, play link for the item will be
    // still available.
    if let Some(play_uri) = play_uri {
        // The URI would be `https://play.dlsite.com/?workno={{workno}}`.
        let prefix_end = play_uri
            .rfind("workno=")
            .map(|v| v + "workno=".len())
            .ok_or_else(|| {
                anyhow::anyhow!("Failed to parse a work ID form the play URI {:?}", play_uri)
            })?;
        return Ok(&play_uri[prefix_end..]);
    }
    log::trace!("Play URI is not available");

    // If the work is unavailable for sale, and also unavailable for play and
    // download, then we can get only thumbnail image (if available), work
    // title, maker name, and maker ID.
    let thumb_uri = row
        .select(Class("work_thumb").descendant(Name("img")))
        .next()
        .and_then(|img| img.attr("src"));
    if let Some(thumb_uri) = thumb_uri {
        // The URI would be
        // `//img.dlsite.jp/resize/images2/work/doujin/{{useless}}/{{workno}}_img_main_240x240.jpg`.
        let (prefix_last, suffix_first) = thumb_uri
            .rfind('/')
            .zip(thumb_uri.find('_'))
            .filter(|(prefix_last, suffix_first)| prefix_last < suffix_first)
            .ok_or_else(|| {
                anyhow::anyhow!(
                    "Failed to parse a work ID from the thumbnail URI {:?}",
                    thumb_uri
                )
            })?;
        return Ok(&thumb_uri[(prefix_last + 1)..suffix_first]);
    }
    log::trace!("Thumbnail URI is not available");

    Err(anyhow::anyhow!("Work ID not found in the row"))
}
