# dlsiter

[![Build Status](https://gitlab.com/nop_thread/dlsiter/badges/develop/pipeline.svg)](https://gitlab.com/nop_thread/dlsiter/pipelines/)
![Minimum supported rustc version: 1.49](https://img.shields.io/badge/rustc-1.49+-lightgray.svg)

A utility for professional DLsiter.

## Config

Create the config file `dlsiter.toml` (this is default name) as below:

```toml
[credential]
session_id = "{{ session_id }}"
```

Substitute your `__DLsite_SID` cookie value for `{{ sesison_id }}`.

## Usage

### `fetch`: Fetch your data from DLsite and save to local DB

Fetches your data from DLsite, and saves them to the local DB.

Argument:

* target: Data to fetch.
    + `works`: Purchased works.

Options:

* `--all`: Fetch all activities, even when some data have already been fetched.
* `--config CONFIG`, `-c CONFIG`: Use config file at the path `CONFIG`.
    + Defaults to `./dlsiter.toml`.
* `--db DB`, `-d DB`: Use DB file at the path `DB`.
    + Defaults to `./dlsiter.sqlite3`.

### `query`: Query filtered data or statistics from the local DB.

Prints filtered data or statistics from the local DB.

Mandatory options:

* `--format FORMAT`, `-f FORMAT`: Use output format `FORMAT`.
    + `csv`: Comma separated values.

Options:

* `--config CONFIG`, `-c CONFIG`: Use config file at the path `CONFIG`.
    + Defaults to `./dlsiter.toml`.
* `--db DB`, `-d DB`: Use DB file at the path `DB`.
    + Defaults to `./dlsiter.sqlite3`.

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
